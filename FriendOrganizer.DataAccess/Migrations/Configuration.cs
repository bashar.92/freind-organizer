namespace FriendOrganizer.DataAccess.Migrations
{
    using FriendOrganizer.Model;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<FriendOrganizer.DataAccess.FriendOrganizerDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(FriendOrganizer.DataAccess.FriendOrganizerDbContext context)
        {
            context.Friends.AddOrUpdate(
                f => f.FirstName,
                new Friend { FirstName = "Bashar", LastName = "Khadra", Email ="bashar@gmail.com" },
                new Friend { FirstName = "Khaled", LastName = "HH", Email = "Khaled@gmail.com" },
                new Friend { FirstName = "Ziad", LastName = "Alsayed", Email = "Ziad@gmail.com" },
                new Friend { FirstName = "Shoubasay", LastName = "Shoubasay", Email = "bakry@gmail.com" }
                );
            context.PogrammingLanguages.AddOrUpdate(
                pl => pl.Name,
                new ProgrammingLanguage { Name = "C#" },
                new ProgrammingLanguage { Name = "TypeScript" },
                new ProgrammingLanguage { Name = "F#" },
                new ProgrammingLanguage { Name = "Swift" },
                new ProgrammingLanguage { Name = "Java" });
            context.FriendPhoneNumber.AddOrUpdate(
                pn => pn.Number,
                new FriendPhoneNumber { Number = "+963 950510721", FriendId = context.Friends.First().Id });
            context.Meetings.AddOrUpdate(m => m.Title,
                new Meeting
                {
                    Title = "Watching Soccer",
                    DateFrom = new DateTime(2018, 5, 26),
                    DateTo = new DateTime(2018, 5, 26),
                    Friends = new List<Friend>
                    {
                        context.Friends.SingleOrDefault(f => f.FirstName == "Bashar" && f.LastName == "Khadra"),
                        context.Friends.SingleOrDefault(f => f.FirstName == "Khaled" && f.LastName == "HH")
                    }
                });
        }
    }
}
